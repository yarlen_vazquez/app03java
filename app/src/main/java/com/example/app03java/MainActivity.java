package com.example.app03java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btnIniciar, btnSalir;
    private EditText txtUsuario, txtContraseña;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeComponents();

        btnIniciar.setOnClickListener(v -> {
            iniciar();
        });

        btnSalir.setOnClickListener(v -> {
            salir();
        });

    }

    private void initializeComponents(){
        btnIniciar = findViewById(R.id.btnIniciar);
        btnSalir = findViewById(R.id.btnSalir);
        txtUsuario = findViewById(R.id.txtUsuario);
        txtContraseña = findViewById(R.id.txtContraseña);
    }

    private void iniciar(){
        String strUsuario= getResources().getString(R.string.usuario);
        String strContraseña = getResources().getString(R.string.contraseña);

        if ((txtUsuario.getText().toString().equals(strUsuario)) && (txtContraseña.getText().toString().equals(strContraseña))){
            Bundle bundle = new Bundle();
            bundle.putString("usuario", strUsuario);

            Intent intent = new Intent(this, CalculadoraActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            return;

        }

        Toast.makeText(MainActivity.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
    }

    private void salir(){
        new AlertDialog.Builder(this)
                .setTitle("Calculadora")
                .setMessage("¿Seguro que quieres salir?")
                .setPositiveButton("Confirmar", (dialog, which) -> finish())
                .setNegativeButton("Cancelar", null)
                .show();
    }
}