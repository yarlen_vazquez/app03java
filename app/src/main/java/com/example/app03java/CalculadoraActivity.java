package com.example.app03java;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {
    private Button btnSumar ;
    private Button btnSubstract;
    private Button btnMultiplicar;
    private Button btnDividir;
    private Button btnLimpiar;
    private Button btnRegresar;
    private EditText txtNum1;
    private EditText txtNum2;
    private TextView lblResultado;

    private final Calculadora calculadora = new Calculadora(0,0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        initializeComponents();

        btnSumar.setOnClickListener(v -> {
            sumar();
        });
        btnSubstract.setOnClickListener(v -> {
            substract();
        });
        btnDividir.setOnClickListener(v -> {
            dividir();
        });
        btnMultiplicar.setOnClickListener(v -> {
            multiplicar();
        });
        btnLimpiar.setOnClickListener(v -> {
            Limpiar();
        });
        btnRegresar.setOnClickListener(v -> {
            btnRegresar();
        });

    }

    private void initializeComponents(){
        btnSumar = findViewById(R.id.btnSumar);
        btnSubstract = findViewById(R.id.btnSubstract);
        btnMultiplicar = findViewById(R.id.btnMultiplicar);
        btnDividir = findViewById(R.id.btnDividir);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        lblResultado = findViewById(R.id.lblResultado);
    }

    private void btnRegresar(){
        new AlertDialog.Builder(this)
                .setTitle("Calculadora")
                .setMessage("¿Seguro que quieres regresar?")
                .setPositiveButton("Confirmar", (dialog, which) -> finish())
                .setNegativeButton("Cancelar", null)
                .show();

    }

    private void Limpiar(){
        this.txtNum1.setText("");
        this.txtNum2.setText("");
        this.lblResultado.setText("");
    }

    private boolean validateFields(){
        if (this.txtNum1.getText().toString().matches("") || this.txtNum2.getText().toString().matches("")){
            Toast.makeText(CalculadoraActivity.this, "Valor requerido", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void setCalculadoraValues(){
        calculadora.setNum1(Integer.parseInt(txtNum1.getText().toString()));
        calculadora.setNum2(Integer.parseInt(txtNum2.getText().toString()));
    }

    private void sumar(){
        if (validateFields()){
            setCalculadoraValues();
            lblResultado.setText(Integer.toString(calculadora.sumar()));
        }
    }

    private void substract(){
        if (validateFields()){
            setCalculadoraValues();
            lblResultado.setText(Integer.toString(calculadora.subtract()));
        }
    }

    private void multiplicar(){
        if (validateFields()){
            setCalculadoraValues();
            lblResultado.setText(Integer.toString(calculadora.multiplicar()));
        }
    }

    private void dividir(){
        if (validateFields()){
            setCalculadoraValues();
            lblResultado.setText(Integer.toString(calculadora.dividir()));
        }
    }


}